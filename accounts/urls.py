from django.urls import path
from accounts.views import create_login, user_logout, SignUp



urlpatterns = [
    path("login/", create_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", SignUp, name="signup")
]
